# Java-Concurrent-Thread-Monitoring
## Purpose
Java program that uses monitors to control access to a FoodBank object. FoodBank class has a food amount and methods to give and take food. Two threads for this program will either put food into the food bank or take food from the food bank. Food cannot be taken if there is no food available to take. This is not a true producer/consumer problem. Only condition is to wait if there is no food available to take. Both actions of giving and taking food will be monitors and will involve locking the lock object and unlocking it when done. The methods to take and give food prints out exactly what is happening with each action in the method, i.e. “Waiting to get food”, “Taking 10 items of food, the balance is now 20 items”.

## A class FoodBank
FoodBank will have a single instance variable named food of type int. FoodBank will define a default constructor which initializes food to zero. FoodBank will have two methods: giveFood and takeFood. Both methods will have a single parameter of type int. giveFood will add the value of the parameter to the food instance variable, takeFood will subtract the value.

## A class FoodProducer
FoodProducer will have a single instance variable named bank of type FoodBank. FoodProducer will have a parameterized constructor with a single parameter of type FoodBank. The parameterized constructor will initialize the value of bank to the single parameter. FoodProducer will extend the Thread class and override Thread’s run method. FoodProducer’s run method will loop infinitely. On each loop iteration run will generate a random number from 1-100 and add that much food to the bank instance variable.

## A class FoodConsumer
FoodConsumer is identical to FoodProducer except that the random number generated in run will be removed from the FoodBank object.