import java.util.Random;

class Giver {
    void run(FoodBank foodBank) {
        Integer i = (new Random()).nextInt(1000);
        foodBank.supply(i);
    }
}