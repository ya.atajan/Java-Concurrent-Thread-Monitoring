import java.util.concurrent.locks.*;

class FoodBank {
    private Lock lock;
    private Condition canSupply;
    private Integer foodRemaining = 0;

    private FoodBank() {
        lock = new ReentrantLock();
        canSupply = lock.newCondition();
    }

    // For readability
    private void sleep() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void supply(Integer supply) {
        lock.lock();
        try {
            System.out.println(String.format("[Total food: %d] Giver: %d items of food supplying.", foodRemaining, supply));
            foodRemaining += supply;
            sleep();
            System.out.println(String.format("[Total food: %d] Giver: %d items of food supplied.", foodRemaining, supply));
            sleep();
            canSupply.signal();
        } finally {
            lock.unlock();
        }
    }

    void demand(Integer demand) {
        lock.lock();
        try {
            while (foodRemaining < demand) {
                System.out.println(String.format("[Total food: %d] Taker: %d items of food demanded.", foodRemaining, demand));
                sleep();
                canSupply.await();
            }
            System.out.println(String.format("[Total food: %d] Taker: %d items of food ready to take. ", foodRemaining, demand));
            foodRemaining -= demand;
            sleep();
            System.out.println(String.format("[Total food: %d] Taker: %d items of food taken.", foodRemaining, demand));
            sleep();
        } catch (InterruptedException exception) {
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        FoodBank foodBank = new FoodBank();
        Giver giver = new Giver();
        Taker taker = new Taker();

        // Conditions
        (new Thread(() -> { while (true) { giver.run(foodBank); } })).start();
        (new Thread(() -> { while (true) { taker.run(foodBank); } })).start();
    }
}